
var image = document.getElementById("slide_image");
var nextbutton = document.getElementById("image_next_button");
var prevbutton = document.getElementById("image_prev_button");
var image_array = ["schaf1.JPG","schaf2.JPG","schaf3.JPG"];
var nextimage;
var image_index = 0;
nextbutton.onclick = function(){
    image_index++;
    if(image_index >= image_array.length){
        image_index = 0;
    }
    nextimage = image_array[image_index];
    image.setAttribute("src",nextimage);
}
prevbutton.onclick = function(){
    image_index--;
    if(image_index <= -1){
        image_index = image_array.length - 1;
    }
    nextimage = image_array[image_index];
    image.setAttribute("src",nextimage);
}